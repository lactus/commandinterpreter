﻿using CommandInterpreter;
using System.Linq;
using CommandInterpreter.Calculator;
using plib.console;

using Console = System.Console;

namespace REPL
{
    class Program
    {
        static void Main(string[] args)
        {
            CmdInterpreter ci = new CmdInterpreter();
            ci.LoadCoreUtils();
            ci.AddProgram<Calc>();

            CoolConsole cc = new CoolConsole();
            cc.AutoCompletions = ci.LoadedCommands.Select(o => o.Name).OrderBy(o => o).ToArray();



            //ci.Initialize(
            //                            () => cc.GetInput(),
            //                            Console.WriteLine,
            //                            o =>
            //                            {
            //                                Console.ForegroundColor = System.ConsoleColor.Red;
            //                                Console.WriteLine(o);
            //                                Console.ResetColor();
            //                            });

            ci.Initialize(
                                        () =>
                                        {
                                            Console.Write(">");
                                            Console.ForegroundColor = System.ConsoleColor.White;
                                            try
                                            {
                                                return Console.ReadLine();
                                            }
                                            finally
                                            {
                                                Console.ResetColor();
                                            }

                                        },
                                        Console.WriteLine,
                                        o =>
                                        {
                                            Console.ForegroundColor = System.ConsoleColor.Red;
                                            Console.WriteLine(o);
                                            Console.ResetColor();
                                        });
            while (true)
            {
                ci.Run();
            }
        }

        /*private static string GetInput(CmdInterpreter _ci)
        {
            Console.Write(">");
            Console.ForegroundColor = System.ConsoleColor.White;
            string text = "";

            System.ConsoleKeyInfo key;
            while ((key = Console.ReadKey()).Key != System.ConsoleKey.Enter)
            {
                bool b;
                text = FoundSpecialKey(text, _ci, key, out b);
                if (!b)
                {
                    text += key.KeyChar;
                }
            }

            try
            {
                Console.WriteLine();
                return text;
            }
            finally
            {
                Console.ResetColor();
            }
        }

        private static string FoundSpecialKey(string _input, CmdInterpreter _ci, System.ConsoleKeyInfo _key, out bool _found)
        {
            string text = _input;
            #region -- Tab --
            if (_key.Key == System.ConsoleKey.Tab)
            {
                IOrderedEnumerable<string> foundCommands;
                foundCommands = _ci.LoadedCommands.Where(o => o.Name.StartsWith(text)).Select(o => o.Name).OrderBy(o => o);
                if (foundCommands.Count() > 1)
                {
                    Console.WriteLine(string.Join("\t", foundCommands));
                }
                else
                {
                    text = foundCommands.First();
                    Console.WriteLine();

                }
                Console.ResetColor();
                Console.Write(">");
                Console.ForegroundColor = System.ConsoleColor.White;
                Console.Write(text);
                _found = true;
                return text;
            }
            #endregion
            #region -- Backspace --
            if (_key.Key == System.ConsoleKey.Backspace)
            {
                text = text.Substring(0, text.Length - 1);
                Console.Write(" ");
                Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                _found = true;
                return text;
            }
            #endregion
            #region -- Cursors ---
            if (_key.Key == System.ConsoleKey.LeftArrow)
            {
                Console.SetCursorPosition(System.Math.Max(Console.CursorLeft - 2, 0), Console.CursorTop);
                _found = true;
                return text;
            }
            #endregion

            _found = false;
            return text;
        }*/
    }
}
