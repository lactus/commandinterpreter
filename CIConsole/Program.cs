﻿using CommandInterpreter;
using CommandInterpreter.Calculator;


namespace CIConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            CmdInterpreter ci = new CmdInterpreter();
            ci.LoadCoreUtils();
            ci.AddProgram<Calc>();

            string commandText = string.Join("", args);
            string output = "";

            ci.Initialize(o => output += o + " ");

            ci.Run("loadv(variables.var)");
            ci.Run(commandText);
            ci.Run("savev(variables.var)");

            if (output.Trim() != string.Empty)
            {
                System.Console.WriteLine(commandText + ": " + output);
            }
        }
    }
}
