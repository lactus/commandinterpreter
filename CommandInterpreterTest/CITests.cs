﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommandInterpreter;
using CommandInterpreter.Calculator;
using System.IO;
using System.Linq;

namespace CommandInterpreterTest
{
    [TestClass]
    public class CITests
    {
        string m_input = "";
        string m_output = "";
        string m_error = "";

        #region --- Command Texts ---
        public static string s_helpCommandOutput = @"REFERENCE
    run a command with <CommandName>([parameter])
    use 'list()' to view all available commands
    use man(<command>) to get help for a command
    you can chain commands by appending a command behind another
    CommandInterpreter has no datatypes, every input is a text
VARIABLES
    you can set variables with the set-command,
    some programms substitute a Variablename with its value
    Variablenames start with $, have more than zero chars, using numbers, 
    capital letters and underscores
    \$[A-Z0-9_]+
    $I cannot be used, since it is already used in iterations
    you can delete variables by setting them to 'alarm'
    if you use a variable without initializing it it will return 'alarm'
INNER COMMANDS
    you can use inner commands by encapsulate them in curly brackets 
    {}
DELEGATES
    by combining inner commands with variables you can do something like this
    >set($HURR,calc(1+2))
    >{echo($HURR)}
    3
POINTERS
    if you are desperate or just mad you can use pointers in this way
    >set($HURR,1)
    >set($DURR,$HURR)
    >echo({echo($DURR)})
    1
EXAMPLES
    list()
        list    set     echo    man     help    calc
    set($TEST, 5)calc($TEST+$TEST)
        10
    set($ALARM,{calc(2+3)})";


        #endregion --- Command Texts ---

        private CmdInterpreter m_cmdi;

        [TestInitialize]
        public void ClassInitialize()
        {
            InitializeCMDI();
        }

        private void PrintVariables()
        {
            Console.WriteLine("--- START ---");
            Console.WriteLine("--- OUTPUT ---");
            Console.WriteLine(m_output);
            Console.WriteLine("--- ERROR ---");
            Console.WriteLine(m_error);
            Console.WriteLine("--- END ---");
        }

        private void InitializeCMDI()
        {
            if (m_cmdi != null)
            {
                return;
            }

            m_cmdi = new CmdInterpreter();
            m_cmdi.LoadCoreUtils();
            m_cmdi.AddProgram(new Calc());
            m_cmdi.Initialize(() => m_input, o => m_output += o, o => m_error += o);
        }

        #region --- Input ---

        private void InvalidOutputs()
        {
            PrintVariables();

            Assert.AreEqual(m_output.Length, 0);
            Assert.AreNotEqual(m_error.Length, 0);
        }

        private void ValidOutputs()
        {
            PrintVariables();

            Assert.AreNotEqual(m_output.Length, 0);
            Assert.AreEqual(m_error.Length, 0);
        }

        private void NoOutputs()
        {
            Assert.AreEqual(m_output.Length, 0);
            Assert.AreEqual(m_error.Length, 0);
        }

        #endregion

        #region --- Test Core Features ---

        [TestMethod]
        public void TestInvalidInputs()
        {
            m_cmdi.Run("fgfgfhgh");

            InvalidOutputs();
        }

        [TestMethod]
        public void TestInvalidInnerCommandInputs1()
        {
            m_cmdi.Run("fgfgfh{gh");

            InvalidOutputs();
        }

        [TestMethod]
        public void TestInvalidInnerCommandInputs2()
        {
            m_cmdi.Run("fgfgfh{gh}");

            InvalidOutputs();
        }

        [TestMethod]
        public void TestEscape()
        {
            m_cmdi.Run(@"echo(\))");

            ValidOutputs();

            Assert.AreEqual(m_output, @"\)");

            m_output = "";
            m_error = "";

            m_cmdi.Run(@"echo(\))");

            ValidOutputs();

            Assert.AreEqual(m_output, @"\)");

            m_output = "";
            m_error = "";

            m_cmdi.Run(@"echo(\{\})");

            ValidOutputs();

            Assert.AreEqual(@"\{\}", m_output);

            m_cmdi.Run(@"echo({echo({)})");

            ValidOutputs();
        }

        [TestMethod]
        public void TestNegation()
        {
            m_cmdi.Run("if(!0,calc(1+1))");

            ValidOutputs();

            Assert.AreEqual(m_output, "2");

            m_output = "";
            m_error = "";

            m_cmdi.Run("if(!!0,calc(1+1))");

            PrintVariables();
            Assert.AreEqual(m_output, "");
            Assert.AreEqual(m_error, "");
        }

        [TestMethod]
        public void TestMultiline()
        {
            m_cmdi.Run(@"calc(
                        1
                        +
                        1)");

            ValidOutputs();

            Assert.AreEqual(m_output, "2");
        }

        [TestMethod]
        public void TestTab()
        {
            m_cmdi.Run("\tcalc(1+\t1)");

            ValidOutputs();

            Assert.AreEqual(m_output, "2");
        }

        #endregion

        #region --- Test Simple Commands ---

        [TestMethod]
        public void TestHelp()
        {
            m_cmdi.Run("help()");

            ValidOutputs();
        }

        [TestMethod]
        public void TestCalc()
        {
            m_cmdi.Run("calc(1+2*3)");

            PrintVariables();

            ValidOutputs();

            Assert.AreEqual(m_output, "7");
        }

        [TestMethod]
        public void TestLogCommand()
        {
            m_cmdi.Run("log()");
            m_cmdi.Run("calc(1+2)");
            m_cmdi.Run("log(test1.log)");

            ValidOutputs();
            Assert.AreEqual(File.Exists("test1.log"), true);
            string text = File.ReadAllText("test1.log");
            Assert.AreNotEqual(text.Length, 0);
            Assert.AreEqual(text, "calc(1+2)");
        }

        [TestMethod]
        public void TestRandom()
        {
            m_cmdi.Run("rand()");

            ValidOutputs();
            Assert.IsTrue(m_output == "0" || m_output == "1");

            m_output = "";
            m_error = "";

            m_cmdi.Run("rand(6)");

            ValidOutputs();
            Assert.IsTrue(int.Parse(m_output) < 6 && int.Parse(m_output) >= 0);

            m_output = "";
            m_error = "";

            m_cmdi.Run("rand(3,6)");

            ValidOutputs();
            Assert.IsTrue(int.Parse(m_output) < 6 && int.Parse(m_output) >= 3);
        }

        [TestMethod]
        public void TestEchoCommand()
        {
            m_cmdi.Run("echo(hurp)");

            ValidOutputs();
            Assert.AreEqual(m_output, "hurp");
        }

        #endregion

        #region --- Test Command Command---

        [TestMethod]
        public void TestCommandCommand()
        {
            m_cmdi.Run("command(unload,calc)");
            ValidOutputs();
            Assert.AreEqual(m_output, "calc unloaded");
            m_cmdi.LoadedCommands.ToList().ForEach(o => Console.WriteLine(o.Name));
            Assert.AreNotEqual(m_cmdi.LoadedCommands.Any(o => o.GetType() == typeof(Calc)), true);

            m_output = "";
            m_error = "";

            m_cmdi.Run("command(load,calc)");

            ValidOutputs();
            Assert.AreEqual(m_output, "calc loaded");
            Assert.AreEqual(m_cmdi.LoadedCommands.Any(o => o.GetType() == typeof(Calc)), true);
        }

        #endregion

        #region --- Test If ---

        [TestMethod]
        public void TestIfCommandTrue()
        {
            m_cmdi.Run("if(true,calc(1+2))");
            ValidOutputs();
            Assert.AreEqual(m_output, "3");
        }

        [TestMethod]
        public void TestIfCommandTrueInnerCommand()
        {
            m_cmdi.Run("if({calc(1+1)},calc(1+2))");
            ValidOutputs();
            Assert.AreEqual(m_output, "3");
        }

        [TestMethod]
        public void TestIfCommandFalse()
        {
            m_cmdi.Run("if(false,calc(1+2))");
            NoOutputs();
        }

        [TestMethod]
        public void TestIfCommandFalseInnerCommand()
        {
            m_cmdi.Run("if({calc(1-1)},calc(1+2))");

            NoOutputs();
        }

        [TestMethod]
        public void TestIfCommandHelp()
        {
            m_cmdi.Run("if(true,help())");
            ValidOutputs();

            Assert.AreEqual(m_output, s_helpCommandOutput);
        }

        #endregion

        #region --- Test For ---

        [TestMethod]
        public void TestForCommandCalc()
        {
            m_cmdi.Run("for(3,calc(1+2))");
            ValidOutputs();
            Assert.AreEqual(m_output, "333");
        }

        [TestMethod]
        public void TestForCommandHelp()
        {
            m_cmdi.Run("for(3,help())");
            ValidOutputs();
            Assert.AreEqual(m_output, s_helpCommandOutput + s_helpCommandOutput + s_helpCommandOutput);
        }

        [TestMethod]
        public void TestForIteration()
        {
            m_cmdi.Run("for(3,echo($I))");
            ValidOutputs();
            Assert.AreEqual(m_output, "012");
        }

        [TestMethod]
        public void TestRestrictedSet()
        {
            m_cmdi.Run("set($I,1)");
            InvalidOutputs();
        }

        #endregion

        #region --- Test Variable SaveLoad ---

        [TestMethod]
        public void TestListVCommand()
        {
            m_cmdi.Run("set($HURR,1)");
            m_cmdi.Run("listv()");

            ValidOutputs();
            Assert.AreEqual(m_output, "$HURR\t1");
        }

        [TestMethod]
        public void TestSaveVariables()
        {
            m_cmdi.AddVariable("$HURP", "durp");
            m_cmdi.Run("savev(variables.vars)");


            PrintVariables();

            Assert.AreEqual(m_output.Length, 0);
            Assert.AreEqual(m_error.Length, 0);

            Assert.AreEqual(File.Exists("variables.vars"), true);
            string text = File.ReadAllText("variables.vars");
            Assert.AreNotEqual(text.Length, 0);
            Assert.AreEqual(text, "$HURP,durp");
        }

        [TestMethod]
        public void TestLoadVariables()
        {
            m_cmdi.ClearVariables();
            m_cmdi.Run("loadv(variables.vars)");

            PrintVariables();

            Assert.AreEqual(m_output.Length, 0);
            Assert.AreEqual(m_error.Length, 0);

            Assert.AreEqual(File.Exists("variables.vars"), true);
            string text = File.ReadAllText("variables.vars");
            Assert.AreNotEqual(text.Length, 0);
            Assert.AreEqual(text, "$HURP,durp");
        }

        [TestMethod]
        public void TestClear()
        {
            m_cmdi.AddVariable("$HURP", "durp");
            m_cmdi.Run("echo($HURP)");

            ValidOutputs();
            Assert.AreEqual(m_output, "durp");

            m_output = "";
            m_error = "";

            m_cmdi.Run("clear()");
            Assert.AreEqual(m_output, "");
            Assert.AreEqual(m_error, "");

            m_output = "";
            m_error = "";

            m_cmdi.Run("echo($HURP)");

            Assert.AreEqual(m_output, "alarm");
            Assert.AreEqual(m_error, "");
        }

        #endregion

        #region --- File IO ---

        [TestMethod]
        public void TestWrite()
        {
            m_cmdi.Run("write(writeTest.txt,calc(1+1))");

            PrintVariables();

            Assert.AreEqual(m_output.Length, 0);
            Assert.AreEqual(m_error.Length, 0);

            Assert.AreEqual(File.Exists("writeTest.txt"), true);
            string text = File.ReadAllText("writeTest.txt");

            Assert.AreEqual(text, "calc(1+1)");
        }

        [TestMethod]
        public void TestLoadCommand()
        {
            m_cmdi.Run("load(test1.log)");

            ValidOutputs();
            Assert.AreEqual(m_output, "3");
        }

        [TestMethod]
        public void TestPrintCommand()
        {
            m_cmdi.Run("print(test1.log)");

            ValidOutputs();
            Assert.AreEqual(m_output, "calc(1+2)");
        }

        #endregion

        #region --- TestInit ---
        [TestMethod]
        public void TestInit()
        {
            CmdInterpreter ci = new CmdInterpreter();
            ci.LoadCoreUtils();
            ci.AddProgram<Calc>();
            string error = "";
            ci.Initialize(null, null, o => error += o);
            string output = ci.Run("calc(1+2)");

            Assert.AreEqual(error, "");
            Assert.AreEqual(output, "3");

        }
        #endregion
    }
}
