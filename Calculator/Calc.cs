﻿using System.Text.RegularExpressions;
using plib.Util;
using System.Collections.Generic;
using CommandInterpreter;

namespace CommandInterpreter.Calculator
{
    /// <summary>
    /// Use this to calculate mathematical expressions
    /// </summary>
    [Command("calc")]
    public class Calc : ACommand
    {
        private const string REGEX_POINT = @"[\.\,]";
        private const string REGEX_NUMBER = @"\d+([\.\,]\d+)?";
        private const string REGEX_OPERATOR = @"[\+\-\*\/\%]{1}";
        private const string REGEX_LEFT_BRACES = @"\(";
        private const string REGEX_RIGHT_BRACES = @"\)";
        private const string REGEX_POW = @"pow\((" + REGEX_NUMBER + @"),+(" + REGEX_NUMBER + @")\)";

        private const string REGEX_START_NUMBER = @"^" + REGEX_NUMBER;
        private const string REGEX_START_OPERATOR_REGEX = @"^" + REGEX_OPERATOR;
        private const string REGEX_START_LEFT_BRACES = @"^" + REGEX_LEFT_BRACES;
        private const string REGEX_START_RIGHT_BRACES = @"^" + REGEX_RIGHT_BRACES;
        private const string REGEX_START_POW = @"^" + REGEX_POW;

        private static System.Globalization.CultureInfo s_cultureInfo = new System.Globalization.CultureInfo("en-US");

        public delegate string RegexAction(string _string, ContainerList _containerList, ref bool _changedSomething);
        public List<RegexAction> m_RegexActions = new List<RegexAction>();

        private Match m_currentMatch;

        public override string ManPage
        {
            get
            {
                return
@"NAME
    calc - calculate a calculation
SYNTAX
    calc(<calculation>)
    takes one parameter, can include subcommands
SUBCOMMANDS
    pow(<base>,<exponent>)
        potentiates base by exponent
EXAMPLES
    calc(1+2)
        3
    calc(2*(1+2.2))
        6.4
    calc(pow(2,3))
        8";
            }
        }

        public override List<string> SubCommands
        {
            get
            {
                return new List<string>()
                {
                    "pow"
                };
            }
        }
        
        public override void Initialize(CmdInterpreter _owner)
        {
            base.Initialize(_owner);

            if (!m_RegexActions.Contains(RegexForRightBraces))
                m_RegexActions.Add(RegexForRightBraces);
            if (!m_RegexActions.Contains(RegexForLeftBraces))
                m_RegexActions.Add(RegexForLeftBraces);
            if (!m_RegexActions.Contains(RegexForPow))
                m_RegexActions.Add(RegexForPow);
            if (!m_RegexActions.Contains(RegexForOperators))
                m_RegexActions.Add(RegexForOperators);
            if (!m_RegexActions.Contains(RegexForNumber))
                m_RegexActions.Add(RegexForNumber);
            if (!m_RegexActions.Contains(RegexForPoint))
                m_RegexActions.Add(RegexForPoint);
        }

        private ContainerList Read(string _input = null)
        {
            // 1. Container erzeugen
            ContainerList cl = new ContainerList();

            // 2. Checken ob Input da ist
            if (_input == null)
            {
                _input = m_owner.InvokeInput();
            }

            // 3. Variablen einsetzen und alles für Debug vorbereiten
            string tmp = _input;
            tmp = m_owner.CheckVariables(tmp);


            // 4. input trimmen um auf leere strings zu checken
            tmp = tmp.Trim();

            // 5. Leseschleife
            bool changedSomething = false;
            while (tmp != string.Empty)
            {

                // 6. jedes mal trimmen
                tmp = tmp.Trim();

                // 7. zeug aus dem string ziehen und den rest zurückgeben (fifo)
                tmp = DoRegexActions(tmp, cl, ref changedSomething);

                // 8. falls der string nicht erkannt wird
                // wird alles kaputt gehen
                if (!changedSomething)
                {
                    char firstChar = tmp[0];
                    m_owner.InvokeError("Error in \"" + _input + "\" at " + 
                        (_input.IndexOf(firstChar) + 1) + ": \""+ firstChar + "\"");
                    cl = null;
                    break;
                }
                changedSomething = false;
            }

            // 9. Ergbniss zurückgeben
            return cl;
        }

        private string DoRegexActions(string _input, ContainerList _cl, ref bool _changedSomething)
        {
            foreach (RegexAction regExAction in m_RegexActions)
            {
                _input = regExAction(_input, _cl, ref _changedSomething);
                if (_changedSomething)
                {
                    break;
                }
            }
            return _input;
        }

        private string RegexForPow(string _input, ContainerList cl, ref bool _changedSomething)
        {
            m_currentMatch = Regex.Match(_input, REGEX_START_POW);
            if (m_currentMatch.Success)
            {
                float value =
                    (float)System.Math.Pow(System.Convert.ToDouble(m_currentMatch.Groups[1].Value, s_cultureInfo),
                                   System.Convert.ToDouble(m_currentMatch.Groups[3].Value, s_cultureInfo));
                cl.Add(new NumberContainer(value));
                _input = _input.SkipFirst(m_currentMatch.Value.Length);
                _changedSomething = true;
            }

            return _input;

        }

        private string RegexForOperators(string _input, ContainerList cl, ref bool _changedSomething)
        {
            m_currentMatch = Regex.Match(_input, REGEX_START_OPERATOR_REGEX);
            if (m_currentMatch.Success)
            {
                cl.Add(new OperatorContainer(m_currentMatch.Value));
                _input = _input.SkipFirst(m_currentMatch.Value.Length);
                _changedSomething = true;
            }

            return _input;
        }

        private string RegexForRightBraces(string _input, ContainerList _cl, ref bool _changedSomething)
        {
            m_currentMatch = Regex.Match(_input, REGEX_START_RIGHT_BRACES);
            if (m_currentMatch.Success)
            {
                _input = _input.SkipFirst(1);
                _changedSomething = true;
            }

            return _input;
        }

        private string RegexForLeftBraces(string _input, ContainerList cl, ref bool _changedSomething)
        {
            //bool braceToRemove = false;
            string bracesString;

            m_currentMatch = Regex.Match(_input, REGEX_START_LEFT_BRACES);
            if (m_currentMatch.Success)
            {
                bracesString = _input.ToBraceEnd();
                cl.Add(Read(bracesString));
                _input = _input.SkipFirst(bracesString.Length + 2); // die 2 wegen den Klammern die im String fehlen
                _changedSomething = true;
            }

            return _input;
        }

        private string RegexForNumber(string _input, ContainerList _cl, ref bool _changedSomething)
        {
            m_currentMatch = Regex.Match(_input, REGEX_START_NUMBER);

            if (m_currentMatch.Success)
            {
                _cl.Add(new NumberContainer(System.Convert.ToSingle(m_currentMatch.Value, s_cultureInfo)));
                _input = _input.SkipFirst(m_currentMatch.Value.Length);
                _changedSomething = true;
            }

            return _input;
        }

        private string RegexForPoint(string _input, ContainerList _cl, ref bool _changedSomething)
        {
            m_currentMatch = Regex.Match(_input, REGEX_POINT);
            if (m_currentMatch.Success)
            {
                _input = _input.SkipFirst(1);
                _changedSomething = true;
            }
            return _input;
        }

        private float Evaluate(ContainerList _list)
        {
            float value = _list.Calculate();

            return value;
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            ContainerList cl = Read(string.Join(",",_arguments));
            if (cl == null)
            {
                return;
            }

            float value = Evaluate(cl);

            m_owner.InvokeOutput(value.ToString());
        }

        /*public string RegexForCommand(string _input, ref bool _changedSomething)
        {
            m_currentMatch = Regex.Match(_input, RegexCommand);
            string output = "";
            if (m_currentMatch.Success)
            {
                string command = _input.SkipFirst(RegexCommand.Length).ToBraceEnd();
                m_outputMethod = (o) => output += o + " ";

                Run(command);
                m_outputMethod = m_originalOutputMethod;
                _changedSomething = true;
            }
            return output;
        }*/
    }
}
