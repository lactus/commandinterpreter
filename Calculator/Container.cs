﻿using System.Collections.Generic;
using System.Linq;

namespace CommandInterpreter.Calculator
{
    public class ContainerList : ValueContainer
    {
        public Container m_First;
        public static Dictionary<string, System.Func<ValueContainer, ValueContainer, NumberContainer>> s_GetOperation =
            new Dictionary<string, System.Func<ValueContainer, ValueContainer, NumberContainer>>()
                            {
                                {
                                    "+", (o1, o2) => { return o1 + o2; }
                                },
                                {
                                    "-", (o1, o2) => { return o1 - o2; }
                                },
                                {
                                    "*", (o1, o2) => { return o1 * o2; }
                                },
                                {
                                    "/", (o1, o2) => { return o1 / o2; }
                                },
                                {
                                    "%", (o1, o2) => { return o1 % o2; }
                                }
                            };
        
        public override float Value
        {
            get
            {
                return Calculate();
            }
        }

        public void Add(Container _container)
        {
            if (m_First == null)
            {
                m_First = _container;
                return;
            }
            Container tmpContainer = m_First;
            while (tmpContainer.m_Next != null)
            {
                tmpContainer = tmpContainer.m_Next;
            }
            tmpContainer.m_Next = _container;
            _container.m_Previous = tmpContainer;
        }

        public float Calculate()
        {
            if (m_First == null)
            {
                return 0;
            }

            OperatorContainer oc;
            Container tmpContainer;

            // Punkt vor Strich
            while ((oc = Find("*", "/", "%")) != null)
            {
                tmpContainer = oc.Calculate();
                m_First = tmpContainer.First;
            }

            // restliches
            while ((oc = Find("+", "-")) != null)
            {
                tmpContainer = oc.Calculate();
                m_First = tmpContainer.First;
            }

            ValueContainer vc = m_First as ValueContainer;
            if (vc == null)
            {
                return float.NaN;
            }

            return vc.Value;
        }

        public OperatorContainer Find(params string[] _operators)
        {
            Container tmpContainer = m_First;
            OperatorContainer oc;

            while (tmpContainer != null)
            {
                oc = tmpContainer as OperatorContainer;
                if (oc != null && _operators.Contains(oc.m_Operator))
                {
                    return oc;
                }

                tmpContainer = tmpContainer.m_Next;
            }

            return null;
        }
    }

    public class Container
    {
        public Container m_Next;
        public Container m_Previous;

        public CmdInterpreter Interpreter { get; protected set; }

        public Container First
        {
            get
            {
                Container tmpContainer = this;

                while (tmpContainer.m_Previous != null)
                {
                    tmpContainer = tmpContainer.m_Previous;
                }

                return tmpContainer;
            }
        }
    }

    public abstract class ValueContainer : Container
    {
        public abstract float Value { get; }

        public static NumberContainer operator +(ValueContainer _first, ValueContainer _second)
        {
            NumberContainer nc = new NumberContainer((_first != null ? _first.Value : 0) + (_second != null ? _second.Value : 0));
            if (_first != null)
            {
                nc.m_Previous = _first.m_Previous;
                if (_first.m_Previous != null)
                {
                    _first.m_Previous.m_Next = nc;
                }
            }
            if (_second != null)
            {
                nc.m_Next = _second.m_Next;
                if (_second.m_Next != null)
                {
                    _second.m_Next.m_Previous = nc;
                }
            }

            return nc;
        }

        public static NumberContainer operator -(ValueContainer _first, ValueContainer _second)
        {
            NumberContainer nc = new NumberContainer(
                (_first != null ? _first.Value : 0) - (_second != null ? _second.Value : 0));

            if (_first != null)
            {
                nc.m_Previous = _first.m_Previous;
                if (_first.m_Previous != null)
                {
                    _first.m_Previous.m_Next = nc;
                }
            }
            if (_second != null)
            {
                nc.m_Next = _second.m_Next;
                if (_second.m_Next != null)
                {
                    _second.m_Next.m_Previous = nc;
                }
            }

            return nc;
        }

        public static NumberContainer operator *(ValueContainer _first, ValueContainer _second)
        {
            NumberContainer nc = new NumberContainer(
                (_first != null ? _first.Value : 0) * (_second != null ? _second.Value : 0));

            if (_first != null)
            {
                nc.m_Previous = _first.m_Previous;
                if (_first.m_Previous != null)
                {
                    _first.m_Previous.m_Next = nc;
                }
            }
            if (_second != null)
            {
                nc.m_Next = _second.m_Next;
                if (_second.m_Next != null)
                {
                    _second.m_Next.m_Previous = nc;
                }
            }

            return nc;
        }

        public static NumberContainer operator /(ValueContainer _first, ValueContainer _second)
        {
            NumberContainer nc = new NumberContainer((_first != null ? _first.Value : 0) / (_second != null ? _second.Value : 0));

            if (_first != null)
            {
                nc.m_Previous = _first.m_Previous;
                if (_first.m_Previous != null)
                {
                    _first.m_Previous.m_Next = nc;
                }
            }
            if (_second != null)
            {
                nc.m_Next = _second.m_Next;
                if (_second.m_Next != null)
                {
                    _second.m_Next.m_Previous = nc;
                }
            }

            return nc;
        }

        public static NumberContainer operator %(ValueContainer _first, ValueContainer _second)
        {
            NumberContainer nc = new NumberContainer( (_first != null ? _first.Value : 0) % (_second != null ? _second.Value : 0));

            if (_first != null)
            {
                nc.m_Previous = _first.m_Previous;
                if (_first.m_Previous != null)
                {
                    _first.m_Previous.m_Next = nc;
                }
            }
            if (_second != null)
            {
                nc.m_Next = _second.m_Next;
                if (_second.m_Next != null)
                {
                    _second.m_Next.m_Previous = nc;
                }
            }

            return nc;
        }
    }

    public class NumberContainer : ValueContainer
    {
        private float m_value;

        public override float Value
        {
            get { return m_value; }
        }
        public NumberContainer(float _value)
        {
            m_value = _value;
        }
    }

    public class OperatorContainer : Container
    {
        public string m_Operator;

        public OperatorContainer(string _operator)
        {
            m_Operator = _operator;
        }

        public NumberContainer Calculate()
        {
            ValueContainer prev = m_Previous as ValueContainer;
            ValueContainer next = m_Next as ValueContainer;

            if (ContainerList.s_GetOperation.ContainsKey(m_Operator))
            {
                return ContainerList.s_GetOperation[m_Operator](prev, next);
            }

            return new NumberContainer(0f);
        }
    }
}
