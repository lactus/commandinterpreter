﻿namespace CommandInterpreter
{
    /// <summary>
    /// Class to store Variables for CommandInterpreter
    /// </summary>
    public class CIVariable
    {
        /// <summary>
        /// Value of this Variable
        /// </summary>
        public string m_Value;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_value"></param>
        public CIVariable(string _value)
        {
            m_Value = _value;
        }

        /// <summary>
        /// String overload
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return m_Value;
        }
    }
}
