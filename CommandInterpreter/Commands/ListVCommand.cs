﻿using System.Collections.Generic;

namespace CommandInterpreter.Commands
{
    [Command("listv")]
    public class ListVCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    vlist - prints all variables and their values
SYNTAX
    vlist()
    takes no parameters
";
            }
        }
        
        /// <summary>
        /// Prints all variables
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 0)
            {
                m_owner.InvokeError("Input needs no parameter");
                return;
            }

            foreach (KeyValuePair<string, CIVariable> v in m_owner.Variables)
            {
                m_owner.InvokeOutput(v.Key + "\t" + v.Value);
            }
        }
    }
}
