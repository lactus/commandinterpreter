﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using plib.Util;

namespace CommandInterpreter.Commands
{
    [Command("log")]
    public class LogCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    log - logs an saves input
SYNTAX
    log()
    log(<filename>)
    takes zero ore one parameter
USAGE
    use log() to start a log
    every command run by CommandInterpreter past this
    will be stored till log(<filename>) is used
    this will write a file containing the input
    ";
            }
        }
        
        private List<string> m_inputs = new List<string>();
        private bool m_logStarted = false;

                /// <summary>
        /// Reads input an performs logging-tasks
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);
            if (_arguments.Length == 0)
            {
                StartLog();
                return;
            }
            else if (_arguments.Length == 1)
            {
                StopLog(_arguments[0]);
                return;
            }

            m_owner.InvokeError("Input needs zero or one parameters");
        }

        private void AddInput(string _input)
        {
            m_inputs.Add(_input);
        }

        private void StartLog()
        {
            if (m_logStarted)
            {
                m_owner.InvokeError("Log already started, use log(<filename>) to stop and save it");
                return;
            }

            m_owner.InputPipe += AddInput;
            m_owner.InvokeOutput("Log started");

            m_logStarted = true;
        }

        private void StopLog(string _filename)
        {
            if (!m_logStarted)
            {
                m_owner.InvokeError("Log was not started, use 'log()' to start it");
            }
            if (!StringHelper.IsFilenameValid(_filename))
            {
                m_owner.InvokeError("Filename was not valid, logging continued");
                return;
            }
            m_inputs.RemoveAt(m_inputs.Count() - 1);
            File.WriteAllText(_filename, string.Join("\n", m_inputs.ToArray()));
            m_logStarted = false;
            m_inputs.Clear();
            m_owner.InputPipe -= AddInput;
            m_owner.InvokeOutput("successfully saved to " + _filename);
        }
    }
}
