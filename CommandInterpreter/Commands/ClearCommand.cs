﻿namespace CommandInterpreter.Commands
{
    [Command("clear")]
    class ClearCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    clear - clears all variables from Memory
SYNTAX
    clear()
    takes no parameters
";
            }
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length != 0)
            {
                m_owner.InvokeError("Input needs no parameters");
                return;
            }

            m_owner.ClearVariables();
        }
    }
}
