﻿using System.IO;
using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("loadv")]
    class LoadVCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    loadv - loads variables from a file
SYNTAX
    loadv(<filename>)
    takes one parameter
";
            }
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length != 1)
            {
                m_owner.InvokeError("Input needs one parameter");
                return;
            }

            string filename = _arguments[0];

            if (!File.Exists(filename))
            {
                m_owner.InvokeError(filename + "was not found, Working Directory: " + Directory.GetCurrentDirectory());
                return;
            }

            string[] lines = File.ReadAllLines(filename);
            string[] seperatedLine;
            string secondPart;
            foreach (string line in lines)
            {
                seperatedLine = line.Split(',');
                secondPart = string.Join(",", seperatedLine.Skip(1).ToArray());
                m_owner.AddVariable(seperatedLine[0], secondPart);
            }
        }
    }
}
