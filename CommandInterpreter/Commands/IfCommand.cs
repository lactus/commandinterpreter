﻿using plib.Util.Logic;

namespace CommandInterpreter.Commands
{
    [Command("if")]
    public class IfCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    if - conditional code execution
SYNTAX
    if(<boolExpression>,<CommandInterpreterCommand>)
    if(<boolExpression>,<CommandInterpreterCommand>,<CommandInterpreterCommand>)
    takes two to three parameters
BOOLEXPRESSION
    every input here other than 'alarm', '0', and 'false'
    will execute the command
    if a second command is specified and the boolexpression returns false the second command
    will be executed
NEGATION
    the boolexpression can be inverted by using ! at the beginning of the string
EXAMPLES
    if(true,calc(1+1))
    >2
    if(false,calc(1+1))
    <no output>
    if(false,calc(1+1),calc(1-1))
    >0";
            }
        }

        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 2 &&
                _arguments.Length != 3)
            {
                m_owner.InvokeError("this command needs two parameters");
                return;
            }

            string innerString;

            _arguments[0] = m_owner.CheckVariables(_arguments[0]);

            try
            {
                bool b = _arguments[0].EvaluateBoolExpression();
                if (BoolExpressions.EvaluateBoolExpression(_arguments[0]))
                {
                    if (_arguments.Length == 3)
                    {
                        innerString = m_owner.RunInnerCommands("{" + _arguments[1] + "}", false);
                        m_owner.InvokeOutput(innerString);
                        return;
                    }
                    innerString = m_owner.RunInnerCommands("{" + _arguments[1] + "}", false);
                    m_owner.InvokeOutput(innerString);
                }
                else
                {
                    if (_arguments.Length == 3)
                    {
                        innerString = m_owner.RunInnerCommands("{" + _arguments[2] + "}", false);
                        m_owner.InvokeOutput(innerString);
                        return;
                    }
                }

            } catch(System.Exception _e) { m_owner.InvokeError(_e.ToString()); }

        }

        
    }
}
