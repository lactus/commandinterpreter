﻿using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("man")]
    public class ManCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return 
@"NAME
    man - displays manual pages for commands
SYNTAX
    man(<programm>)
    takes only one parameter";
            }
        }
        
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 1)
            {
                m_owner.InvokeError("Input needs one argument");
                return;
            }

            string cmdText = _arguments[0].Trim();
            ACommand cmd = m_owner.LoadedCommands.FirstOrDefault(o => o.Name == cmdText);
            if (cmd == null)
            {
                m_owner.InvokeError("No manual entry for " + cmdText);
                return;
            }

            m_owner.InvokeOutput(cmd.ManPage);
        }
    }
}
