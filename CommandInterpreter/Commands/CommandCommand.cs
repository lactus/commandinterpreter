﻿using System.Linq;
using plib.Util;

namespace CommandInterpreter.Commands
{
    /// <summary>
    /// Use this command to load and unload commands
    /// </summary>
    [Command("command")]
    public class CommandCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return 
@"NAME
    command - loads and unloads commands
SYNTAX
    command(load,<commandName>)
    command(unload,<commandName>)
USAGE
    use this command to load and unload commands in runtime
    command must be present in assembly
";
            }
        }

        /// <summary>
        /// Reads input an performs logging-tasks
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 2)
            {
                m_owner.InvokeError("Input needs two parameters");
                return;
            }

            if (_arguments[0] == "load")
            {
                Load(_arguments[1]);
            }
            else if (_arguments[0] == "unload")
            {
                Unload(_arguments[1]);
            }
            else
            {
                m_owner.InvokeError(_arguments[0] + "is no valid parameter for command(), try 'load' or 'unload'");
            }
        }

        private void Load(string _commandName)
        {
            if (m_owner.LoadedCommands.Any(o => o.Name == _commandName))
            {
                m_owner.InvokeError(_commandName + "already loaded");
                return;
            }

            var allCommandTypes = GenericHelper.AllSubTypes(typeof(ACommand));
            var groupedCommandTs = allCommandTypes.Select(o => new
                                {
                                    Att = (CommandAttribute) System.Attribute.GetCustomAttribute(o,
                                                                                                typeof(CommandAttribute)),
                                    CommandT = o
                                }).Where(o => o.Att != null);

            var foundCommandT = groupedCommandTs.FirstOrDefault(o => o.Att.Name == _commandName);

            if (foundCommandT == null)
            {
                m_owner.InvokeError("No command with this name found");
            }

            ACommand command = (ACommand)System.Activator.CreateInstance(foundCommandT.CommandT);
            m_owner.AddProgram(command);
            command.Initialize(m_owner);

            m_owner.InvokeOutput(_commandName + " loaded");
        }

        private void Unload(string _commandName)
        {
            ACommand command = m_owner.LoadedCommands.FirstOrDefault(o => o.Name == _commandName);

            if (command == null)
            {
                m_owner.InvokeError("no command with name '" + _commandName + "' loaded");
                return;
            }

            m_owner.RemoveProgram(command);

            m_owner.InvokeOutput(_commandName + " unloaded");
        }
    }
}
