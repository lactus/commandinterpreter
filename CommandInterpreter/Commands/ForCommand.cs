﻿using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("for")]
    public class ForCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    for - iterated code execution
SYNTAX
    for(<number>,<CommandInterpreterCommand>)
    takes two parameters
ITERATOR
    for uses the iterator variable $I, it will be automatically set on every iteration
    you can use this variable readonly
EXAMPLES
    for(3,calc(1+1))
    >2
    >2
    >2
    for(3,echo($I))
    >0
    >1
    >2
";
            }
        }

        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 2)
            {
                m_owner.InvokeError("this command needs two parameters");
                return;
            }

            int number;
            if (!int.TryParse(m_owner.CheckVariables(_arguments[0]), out number))
            {
                m_owner.InvokeError("First input is no number: " + _arguments[0]);
                return;
            }

            string commandText = "{" + _arguments[1] + "}";
            string innerOutput;


            for (int i = 0; i < number; i++)
            {
                m_owner.AddVariable("$I", i.ToString(), true);

                innerOutput = m_owner.RunInnerCommands(commandText, false);
                m_owner.InvokeOutput(innerOutput);
            }
        }
    }
}
