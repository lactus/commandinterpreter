﻿namespace CommandInterpreter
{
    /// <summary>
    /// Use this attribute to name every Command
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class CommandAttribute : System.Attribute
    {
        /// <summary>
        /// Name of this Command
        /// </summary>
        public string Name;

        /// <summary>
        /// Constructor for attribute
        /// </summary>
        /// <param name="_name"></param>
        public CommandAttribute(string _name)
        {
            Name = _name;
        }
    }
}
