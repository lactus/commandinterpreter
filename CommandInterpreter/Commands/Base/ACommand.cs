﻿using System.Collections.Generic;

namespace CommandInterpreter
{
    /// <summary>
    /// CommandInterface, use this for CommandInterpreter commands
    /// </summary>
    public abstract class ACommand
    {
        /// <summary>
        /// Name of this command. Don't start with ^
        /// </summary>
        public string Name
        {
            get
            {
                if (m_name == null)
                {
                    CommandAttribute atr = (CommandAttribute)System.Attribute.GetCustomAttribute(GetType(), typeof(CommandAttribute));
                    if (atr == null)
                    {
                        throw new System.Exception(GetType() + ": CommandAttribute is missing!");
                    }
                    m_name = atr.Name;
                }
                return m_name;
            }
        }

        /// <summary>
        /// Returns ManPage of this Command
        /// </summary>
        public abstract string ManPage { get; }

        /// <summary>
        /// All SubCommands
        /// </summary>
        public virtual List<string> SubCommands { get { return new List<string>(); } }       // HACK

        /// <summary>
        /// Is this element marked for deletion? it will be removed on the next call of CmdInterpreter.Run()
        /// </summary>
        public bool MarkedForDeletion { get ; set; }
        /// <summary>
        /// Backlink to CommandInterpreter
        /// </summary>
        protected CmdInterpreter m_owner;

        /// <summary>
        /// Will be set if Initialize was run
        /// </summary>
        protected bool m_initialized;


        private string m_name;


        /// <summary>
        /// Run the Command, initialize before
        /// </summary>
        /// <param name="_arguments"></param>
        public virtual void Run(string[] _arguments)
        {
            if (!m_initialized)
            {
                throw new System.SystemException(Name + ": CommandInterpreter must be initialized before!");
            }
        }

        /// <summary>
        /// Initialize Command
        /// </summary>
        /// <param name="_owner"></param>
        public virtual void Initialize(CmdInterpreter _owner)
        {
            m_owner = _owner;
            m_initialized = true;
        }


    }

}
