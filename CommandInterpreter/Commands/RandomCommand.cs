﻿using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("rand")]
    class RandomCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    rand - returns random value
SYNTAX
    rand()
    rand(<number>)
    rand(<number>, <number>)
    takes zero to two parameters
USAGE
    if no parameter is passed output will be random 0 or 1
    if one parameter is passed output will be random 0 - <firstParam - 1>
    if two parameter is passed output will be random <firstParam> - <secondParam - 1>
EXAMPLES
    rand()
    ><random 0 or 1>
    rand(4)
    ><random 0-3>
    rand(1,3)
    ><random 1-4>
";
            }
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length > 2)
            {
                m_owner.InvokeError("Input needs zero to two parameters");
                return;
            }

            System.Random rnd = new System.Random(System.Guid.NewGuid().GetHashCode());
            if (_arguments.Length == 0)
            {
                m_owner.InvokeOutput(rnd.Next(2).ToString());
                return;
            }

            int[] iArguments;
            try
            {
                iArguments = _arguments.Select(o => int.Parse(o)).ToArray();
            }
            catch
            {
                m_owner.InvokeError("arguments cannot be used as number");
                return;
            }

            if (iArguments.Length == 1)
            {
                m_owner.InvokeOutput(rnd.Next(iArguments[0]).ToString());
                return;
            }

            m_owner.InvokeOutput(rnd.Next(iArguments[0], iArguments[1]).ToString());
        }
    }
}
