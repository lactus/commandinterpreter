﻿using System.IO;

namespace CommandInterpreter.Commands
{
    [Command("load")]
    public class LoadCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    load - loads CommandInterpreter scripts
SYNTAX
    load(<filename>)
    takes one one parameter
USAGE
    if the file is a valid CommandInterpreter script
    it will execute all of them
    use plaintext files
";
            }
        }
        
        /// <summary>
        /// Executes all Commands in a file
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);


            if (_arguments.Length != 1)
            {
                m_owner.InvokeError("Input needs one parameter");
                return;
            }

            string filename = _arguments[0];

            if (!File.Exists(filename))
            {
                m_owner.InvokeError(filename + "was not found");
                return;
            }
            string filetext = "";

            CmdInterpreter innerCI = m_owner.Clone();
            innerCI.Initialize(() => filetext, o => m_owner.InvokeOutput(o), o => m_owner.InvokeError(o));

            string[] lines = File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                innerCI.Run(line);
            }
        }
    }
}
