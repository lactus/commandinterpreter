﻿using System.IO;
using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("write")]
    class WriteCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    write - writes text to a file
SYNTAX
    write(<filename>,<text>)
    takes two parameters
";
            }
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length != 2)
            {
                m_owner.InvokeError("Input needs two parameters");
                return;
            }

            File.WriteAllText(_arguments[0], _arguments[1]);
        }
    }
}
