﻿using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("set")]
    public class SetCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    set - set a variable
SYNTAX
    set(<variableName>,<variableValue>)
    takes two parameters";
            }
        }
        
        /// <summary>
        /// Reads input an performs variable setting
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length != 2)
            {
                m_owner.InvokeError("Input needs two arguments");
                return;
            }

            m_owner.AddVariable(_arguments[0], _arguments[1]);
        }
    }
}
