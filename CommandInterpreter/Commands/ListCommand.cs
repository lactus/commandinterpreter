﻿using System.Collections.Generic;
using System.Linq;

namespace CommandInterpreter.Commands
{
    /// <summary>
    /// Prints all loaded commands
    /// </summary>
    [Command("list")]
    public class ListCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    list - list available commands
SYNTAX
    list()
    takes no parameters";
            }
        }

        /// <summary>
        /// Prints all loaded commands
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 0)
            {
                m_owner.InvokeError("Input needs no parameter");
                return;
            }

            string output = string.Join("\t", m_owner.LoadedCommands.Select(o => o.Name).OrderBy(o => o).ToArray());

            m_owner.InvokeOutput(output);
        }
    }
}
