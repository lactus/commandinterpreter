﻿namespace CommandInterpreter.Commands
{
    [Command("help")]
    public class HelpCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    help - provide help for CommandInterpreter
SYNTAX
    help()
    takes no parameters";
            }
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length != 0)
            {
                m_owner.InvokeError("Input needs no parameter");
                return;
            }

            string text =
@"REFERENCE
    run a command with <CommandName>([parameter])
    use 'list()' to view all available commands
    use man(<command>) to get help for a command
    you can chain commands by appending a command behind another
    CommandInterpreter has no datatypes, every input is a text
VARIABLES
    you can set variables with the set-command,
    some programms substitute a Variablename with its value
    Variablenames start with $, have more than zero chars, using numbers, 
    capital letters and underscores
    \$[A-Z0-9_]+
    $I cannot be used, since it is already used in iterations
    you can delete variables by setting them to 'alarm'
    if you use a variable without initializing it it will return 'alarm'
INNER COMMANDS
    you can use inner commands by encapsulate them in curly brackets 
    {}
DELEGATES
    by combining inner commands with variables you can do something like this
    >set($HURR,calc(1+2))
    >{echo($HURR)}
    3
POINTERS
    if you are desperate or just mad you can use pointers in this way
    >set($HURR,1)
    >set($DURR,$HURR)
    >echo({echo($DURR)})
    1
EXAMPLES
    list()
        list    set     echo    man     help    calc
    set($TEST, 5)calc($TEST+$TEST)
        10
    set($ALARM,{calc(2+3)})";

            m_owner.InvokeOutput(text);
        }
    }
}
