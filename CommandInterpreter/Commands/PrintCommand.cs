﻿using System.IO;

namespace CommandInterpreter.Commands
{
    [Command("print")]
    public class PrintCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    print - prints a textfile
SYNTAX
    print(<filename>)
    takes one parameter
USAGE
    prints input file to console out
";
            }
        }
        
        /// <summary>
        /// prints a file
        /// </summary>
        /// <param name="_arguments"></param>
        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 1)
            {
                m_owner.InvokeError("Input needs one parameter");
                return;
            }

            string filename = _arguments[0];

            if (!File.Exists(filename))
            {
                m_owner.InvokeError(filename + "was not found");
                return;
            }

            string[] lines = File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                m_owner.InvokeOutput(line);
            }
        }
    }
}
