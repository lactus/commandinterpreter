﻿namespace CommandInterpreter.Commands
{
    /// <summary>
    /// This commands echos text to output, substitutes variables
    /// </summary>
    [Command("echo")]
    public class EchoCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    echo - substitute input and return input
SYNTAX
    echo(<input>,...)
    takes minimum one parameter";
            }
        }

        public override void Run(string[] _arguments = null)
        {
            base.Run(_arguments);

            if (_arguments.Length != 1)
            {
                m_owner.InvokeError("Input needs one parameter");
                return;
            }

            _arguments[0] = m_owner.CheckVariables(_arguments[0]);

            m_owner.InvokeOutput(_arguments[0]);
        }
    }
}
