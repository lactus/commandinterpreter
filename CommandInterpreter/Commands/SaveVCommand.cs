﻿using plib.Util;
using System.IO;
using System.Linq;

namespace CommandInterpreter.Commands
{
    [Command("savev")]
    class SaveVCommand : ACommand
    {
        public override string ManPage
        {
            get
            {
                return
@"NAME
    savev - saves variables to a file
SYNTAX
    savev(<filename>)
    takes one parameter
";
            }
        }

        public override void Run(string[] _arguments)
        {
            base.Run(_arguments);

            if (_arguments.Length != 1)
            {
                m_owner.InvokeError("Input needs one parameter");
                return;
            }

            string filename = _arguments[0];
            string text = "";
            m_owner.Variables.Where(o => !m_owner.IsVariableNameRestricted(o.Key)).
                ForEach(o => text += o.Key + "," + o.Value + "\n");

            text = text.Trim();

            File.WriteAllText(filename, text);
        }
    }
}
