﻿using System.Collections.Generic;
using System.Linq;
using plib.Util;
using System.Text.RegularExpressions;
using CommandInterpreter.Commands;

using FuncS = System.Func<string>;
using ActionS = System.Action<string>;

namespace CommandInterpreter
{
    #region --- Delegates ---
    /// <summary>
    /// Delegate to run regex find an replace in CI
    /// </summary>
    /// <param name="_string"></param>
    /// <param name="_changedSomething"></param>
    /// <returns></returns>
    public delegate string CommandAction(string _string, ref bool _changedSomething);
    #endregion

    /// <summary>
    /// Class to controll CI, use new to generate a new CI
    /// </summary>
    public class CmdInterpreter
    {
        #region --- Variables for Variables ---
        /// <summary>
        /// Regex to find valid variable name
        /// </summary>
        public const string VARIABLE_REGEX = @"\$[A-Z0-9_]+";

        /// <summary>
        /// Readonly variable getter
        /// </summary>
        public Dictionary<string, CIVariable> Variables { get { return new Dictionary<string, CIVariable>(m_variables); } }

        // Variablenames start with $, have more than zero chars, using numbers, capital letters and underscores
        // \$[A-Z0-9_]+
        private Dictionary<string, CIVariable> m_variables = new Dictionary<string, CIVariable>();
        #endregion

        #region --- Variables for Commands ---
        /// <summary>
        /// Readonly, do not add stuff here
        /// </summary>
        public LinkedList<ACommand> LoadedCommands
        {
            get
            {
                return new LinkedList<ACommand>(m_commands.Where(o => !o.MarkedForDeletion));
            }
        }

        private LinkedList<ACommand> m_commands = new LinkedList<ACommand>();
        #endregion

        #region --- IO Events ---
        /// <summary>
        /// This is where Run takes it input if none is passed by argument
        /// </summary>
        public FuncS Input { get; private set; }
        /// <summary>
        /// Is called on every input
        /// </summary>
        public event ActionS InputPipe;
        /// <summary>
        /// Is called on every output, can be called if no errormethod is specified
        /// </summary>
        public event ActionS Output;
        /// <summary>
        /// Is called on every error
        /// </summary>
        public event ActionS Error;
        #endregion

        #region --- Private Variables
        private Match m_currentMatch;
        private bool m_initialized;

        private string m_currentSessionOutput = "";
        #endregion

        #region --- Construction and Initialization
        /// <summary>
        /// Creates a new CommandInterpreter use AddProgram() and Initialize() to Run() it
        /// </summary>
        public CmdInterpreter()
        {
        }

        /// <summary>
        /// Clones Commands and Variables
        /// </summary>
        /// <returns></returns>
        public CmdInterpreter Clone()
        {
            if (!m_initialized)
            {
                throw new System.InvalidOperationException("CommandInterpreter must be initialized before");
            }

            CmdInterpreter ci = new CmdInterpreter();
            LoadedCommands.ForEach(o => ci.AddProgram((ACommand)System.Activator.CreateInstance(o.GetType())));
            m_variables.ForEach(o => ci.AddVariable(o.Key, o.Value.m_Value, true));

            return ci;
        }

        /// <summary>
        /// Initializes all commands, use this method whenever you add new Commands
        /// Use this if you intend to run Run() with a string-parameter
        /// </summary>
        /// <param name="_outputMethod"></param>
        /// <param name="_errorMethod"></param>
        public void Initialize(ActionS _outputMethod, ActionS _errorMethod = null)
        {
            Initialize(null, _outputMethod, _errorMethod);
        }

        /// <summary>
        /// Initializes all commands, use this method whenever you add new Commands
        /// </summary>
        /// <param name="_inputMethod"></param>
        /// <param name="_outputMethod"></param>
        /// <param name="_errorMethod"></param>
        public void Initialize(FuncS _inputMethod, ActionS _outputMethod, ActionS _errorMethod = null)
        {
            /*if (_outputMethod == null)
            {
                throw new System.ArgumentException("output may not be null");
            }*/

            Input = _inputMethod;
            Output = null;
            Output += _outputMethod;
            Error = null;
            Error += _errorMethod;


            m_commands.ForEach(o => o.Initialize(this));
            m_initialized = true;
        }
        #endregion

        #region --- Command Handling ---
        /// <summary>
        /// Loads clear, command, echo, for, help, if, list, listv, load, loadv, log, rand, savev, set, man, print, write
        /// </summary>
        public void LoadCoreUtils()
        {
            AddProgram<ClearCommand>();
            AddProgram<CommandCommand>();
            AddProgram<EchoCommand>();
            AddProgram<ForCommand>();
            AddProgram<HelpCommand>();
            AddProgram<IfCommand>();
            AddProgram<ListCommand>();
            AddProgram<ListVCommand>();
            AddProgram<LoadCommand>();
            AddProgram<LoadVCommand>();
            AddProgram<LogCommand>();
            AddProgram<RandomCommand>();
            AddProgram<SaveVCommand>();
            AddProgram<SetCommand>();
            AddProgram<ManCommand>();
            AddProgram<PrintCommand>();
            AddProgram<WriteCommand>();
        }

        /// <summary>
        /// Adds a program to commandInterpreter, remember to use Initialize
        /// </summary>
        /// <param name="_command"></param>
        public void AddProgram(ACommand _command)
        {
            if (!m_commands.Any(o => o.GetType() == _command.GetType()))
            {
                m_commands.AddLast(_command);
            }
        }

        /// <summary>
        /// Adds a program to commandInterpreter, remember to use Initialize
        /// </summary>
        public void AddProgram<T>() where T : ACommand, new()
        {
            if (!m_commands.Any(o => o.GetType() == typeof(T)))
            {
                m_commands.AddLast(new T());
            }
        }

        /// <summary>
        /// Removes a Command from CommandInterpreter
        /// </summary>
        /// <param name="_command"></param>
        public void RemoveProgram(ACommand _command)
        {
            ACommand found = m_commands.FirstOrDefault(o => o.GetType() == _command.GetType());

            if (found != null)
            {
                found.MarkedForDeletion = true;
            }
        }

        /// <summary>
        /// Removes a Command from CommandInterpreter
        /// </summary>
        public void RemoveProgram<T>() where T : ACommand
        {
            ACommand found = m_commands.FirstOrDefault(o => o.GetType() == typeof(T));

            if (found != null)
            {
                found.MarkedForDeletion = true;
            }
        }

        private void DeleteRemovedPrograms()
        {
            LinkedList<ACommand> commandsToDelete = new LinkedList<ACommand>(m_commands.Where(o => o.MarkedForDeletion));
            commandsToDelete.ForEach(o => m_commands.Remove(o));
        }
        #endregion

        #region --- Run ---
        /// <summary>
        /// Executes the CommandInterpreter
        /// </summary>
        /// <param name="_input">can be null, if null inputmethod will be used</param>
        /// <param name="_searchForInnerCommands">shall be searched for inner commands</param>
        public string Run(string _input = null, bool _searchForInnerCommands = true)
        {
            try
            {
                DeleteRemovedPrograms();

                if (!m_initialized)
                {
                    throw new System.InvalidOperationException("CommandInterpreter must be initialized before");
                }

                if (_input == null)
                {
                    _input = InvokeInput();
                }
                else if (InputPipe != null)
                {
                    InputPipe(_input);
                }

                _input = _input.Replace("\n", " ");
                _input = _input.Replace("\t", " ");
                _input = _input.Replace("\r", " ");


                bool changedSomething = false;

                if (_searchForInnerCommands)
                {
                    _input = RunInnerCommands(_input);
                }

                while (_input != string.Empty)
                {
                    _input = DoRegexCommands(_input, ref changedSomething);

                    if (!changedSomething)
                    {
                        if (_input.StartsWith(" ") || _input.EndsWith(" "))
                        {
                            _input = _input.Trim();
                        }
                        else if (_input.StartsWith(@"\")
                                && _input.Length >= 2)
                        {
                            _input = _input.Substring(2);
                        }
                        else if (!string.IsNullOrEmpty(_input))
                        {
                            InvokeError("FAILED TO FIND COMMAND: " + _input + "\nTry help()");
                            break;          // ungültige Werte werden zurückgewiesen
                        }
                    }
                    changedSomething = false;
                }


                return m_currentSessionOutput;
            }
            catch (System.Exception _ex)
            {
                InvokeError(_ex.Message);
                InvokeError(_ex.StackTrace);

                return m_currentSessionOutput;
            }
            finally
            {
                m_currentSessionOutput = "";
            }
        }

        /// <summary>
        /// Runs inner CommandInterpreterCommands
        /// </summary>
        /// <param name="_input">Put input in curly braces</param>
        /// <param name="_checkForInnerCommands">shall be searched for inner commands in this command</param>
        /// <returns></returns>
        public string RunInnerCommands(string _input, bool _checkForInnerCommands = true)
        {
            string innerCommand = "";
            string output = "";
            Match match;

            CmdInterpreter innerCI = Clone();
            innerCI.Initialize(() => innerCommand, o => output += o, Error);

            int firstPos;
            string innerBrackets;
            bool hasClosingBrackes;

            while ((match = Regex.Match(_input, @"(^|[^\\]){")) != null && match.Value != "")
            {
                firstPos = match.Index + System.Math.Max(match.Length - 1, 0);
                innerBrackets = _input.Substring(firstPos).ToBraceEnd(out hasClosingBrackes, @"\",StringHelper.EBracketType.CURLY);
                if (!hasClosingBrackes)
                {
                    break;
                }
                innerCI.Run(innerBrackets, _checkForInnerCommands);
                _input = _input.Replace("{" + innerBrackets + "}", output);
                //_input = _input.Escape("{}");
                output = "";

                if (!_checkForInnerCommands)
                {
                    break;
                }
            }

            // Alle Variablen aus Unterprogramm laden, falls dort was geänder wurde
            m_variables.Clear();
            innerCI.m_variables.ForEach(o => AddVariable(o.Key, o.Value.m_Value, true));

            // Commands mit Unterprogramm abgleichen
            var commandsToAdd = innerCI.m_commands.Where(o => !m_commands.Any(p => p.GetType() == o.GetType()));
            var newCommands = commandsToAdd.Select(o => ((ACommand)System.Activator.CreateInstance(o.GetType())));

            newCommands.ForEach(o => AddProgram(o));
            newCommands.ForEach(o => o.Initialize(this));

            var commandsToRemove = m_commands.Where(o => !innerCI.m_commands.Any(p => p.GetType() == o.GetType()));

            commandsToRemove.ForEach(o => RemoveProgram(o));

            return _input;
        }

        private string DoRegexCommands(string _input, ref bool _changedSomething)
        {
            string commandText = "";
            string[] arguments;
            string regexCommand;
            bool closingBrackets;

            int openingCount;
            int closingCount;

            foreach (ACommand command in m_commands.Where(o => !o.MarkedForDeletion))
            {
                regexCommand = command.Name + @"\(";
                m_currentMatch = Regex.Match(_input, @"^" + regexCommand);
                //output = "";
                if (m_currentMatch.Success)
                {
                    commandText = _input.SkipFirst(regexCommand.Length - 2);
                    _input = commandText;
                    if (commandText.FirstOrDefault() == '(')
                    {
                        commandText = commandText.ToBraceEnd(out closingBrackets, "\\");
                        if (!closingBrackets)
                        {
                            InvokeError("Closing bracket could not be found!");
                            return "";
                        }

                        openingCount = Regex.Matches(commandText, @"(?<!\\)\(").Count;
                        closingCount = Regex.Matches(commandText, @"(?<!\\)\)").Count;
                        int escapedNonMatchingClosingBrackets = openingCount - closingCount;
                        _input = _input.SkipFirst(commandText.Length + 2 + escapedNonMatchingClosingBrackets);
                    }
                    //m_outputMethod = (o) => output += o + " ";

                    //commandText = commandText.Unescape(@"\");

                    arguments = commandText.Parameterize();

                    //for (int i = 0; i < arguments.Length; i++)
                    //{
                    //    arguments[i] = arguments[i].Unescape(@"\");
                    //}

                    command.Run(arguments);
                    _changedSomething = true;
                    //command.ResetCommands();
                    break;
                }
            }
            return _input;
        }
        #endregion

        #region --- Variable Handling ---
        /// <summary>
        /// Check for variables in input string
        /// returns input string with variable replacement
        /// if variable name does not exist it will be defaulted to empty string
        /// </summary>
        /// <param name="_input"></param>
        /// <returns></returns>
        public string CheckVariables(string _input)
        {
            MatchCollection matches = Regex.Matches(_input, VARIABLE_REGEX);
            foreach (Match match in matches)
            {
                if (m_variables.ContainsKey(match.Value))
                {
                    _input = _input.Replace(match.Value, m_variables[match.Value].ToString());
                }
                else
                {
                    _input = _input.Replace(match.Value, "alarm");
                }
            }

            return _input;
        }

        /// <summary>
        /// Add or changes a variable
        /// </summary>
        /// <param name="_key">variable name</param>
        /// <param name="_value">variable value</param>
        /// <param name="_allowRestrictedNames">can variable name be a restricted name?</param>
        public void AddVariable(string _key, string _value, bool _allowRestrictedNames = false)
        {
            if (string.IsNullOrEmpty(_key))
            {
                InvokeError("variable name is empty");
                return;
            }

            if (string.IsNullOrEmpty(_value))
            {
                InvokeError("variable value is empty");
                return;
            }

            string formattedKey = Regex.Match(_key, VARIABLE_REGEX).Value;
            if (formattedKey == string.Empty && m_initialized)
            {
                InvokeError(_key + @" was not properly formatted, it should look like " + VARIABLE_REGEX);
                return;
            }

            if (!_allowRestrictedNames)
            {
                if (IsVariableNameRestricted(_key))
                {
                    InvokeError("You can't use " + _key + " as variable Name, it is restricted");
                }

            }

            if (m_variables.ContainsKey(formattedKey))
            {
                if (_value == "alarm")
                {
                    m_variables.Remove(formattedKey);
                }
                else
                {
                    m_variables[formattedKey].m_Value = _value;
                }
                return;
            }
            if (_value != "alarm")
            {
                m_variables.Add(formattedKey, new CIVariable(_value));
            }
        }

        /// <summary>
        /// Clears all variables
        /// </summary>
        public void ClearVariables()
        {
            m_variables.Clear();
        } 

        /// <summary>
        /// Is this variable name restricted?
        /// if one internal variable name is used it will return true
        /// </summary>
        /// <param name="_name"></param>
        /// <returns></returns>
        public bool IsVariableNameRestricted(string _name)
        {
            if (_name == "$I")
            {
                return true;
            }

            return false;
        }
        #endregion

        #region --- IO Handling ---
        /// <summary>
        /// Use this to read input
        /// </summary>
        /// <returns></returns>
        public string InvokeInput()
        {
            string input = Input();

            if (InputPipe != null)
            {
                InputPipe(input);
            }

            return input;
        }

        /// <summary>
        /// Use this to write output
        /// </summary>
        /// <param name="_output"></param>
        public void InvokeOutput(string _output)
        {
            m_currentSessionOutput += _output;

            if (Output != null)
            {
                Output(_output);
            }
        }

        /// <summary>
        /// Use this to write error, if no error is specified it will use output
        /// </summary>
        /// <param name="_error"></param>
        public void InvokeError(string _error)
        {
            if (Error != null)
            {
                Error(_error);
                return;
            }
            if (Output != null)
            {
                Output(_error);
            }
        }
        #endregion
    }
}